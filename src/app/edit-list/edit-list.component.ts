import { Component, OnInit } from '@angular/core';
import { ListService } from '../services/list.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.css']
})

export class EditListComponent implements OnInit {

  lists : any[]; //création d'une liste vide
  error = '';
  listeSubscription : Subscription; 

  //utilise le service
  constructor(private listService: ListService) { }

  ngOnInit() {
    //création de la subscription en souscrivant (subscribe) au subject créé dans le service //équivaut à l'ancienne fonction getList()
    this.listeSubscription = this.listService.listSubject.subscribe(
      (listes : any[]) => { this.lists = listes; }//il emet un array de type listes et on remplit le tableau créé ci-dessus
    
    );
    this.listService.emitListes();//puis on émet le subject pr pouvoir afficher
  }
}





