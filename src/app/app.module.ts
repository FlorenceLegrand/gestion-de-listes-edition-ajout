import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { EditListComponent } from './edit-list/edit-list.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ListService } from './services/list.service';
import { ListAddFormComponent } from './list-add-form/list-add-form.component';
import { EditOneListComponent } from './edit-one-list/edit-one-list.component';

const routes = [
  { path: 'list/add', component : ListAddFormComponent},
  { path: 'list', component : EditListComponent},
  { path: 'list/:id', component : EditOneListComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    EditListComponent,
    ListAddFormComponent,
    EditOneListComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
