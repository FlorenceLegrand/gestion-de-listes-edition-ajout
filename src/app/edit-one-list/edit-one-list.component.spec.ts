import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOneListComponent } from './edit-one-list.component';

describe('EditOneListComponent', () => {
  let component: EditOneListComponent;
  let fixture: ComponentFixture<EditOneListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOneListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOneListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
