import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ListService } from '../services/list.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-one-list',
  templateUrl: './edit-one-list.component.html',
  styleUrls: ['./edit-one-list.component.css']
})
export class EditOneListComponent implements OnInit {
  listId : string; //pr récupérer l'id de la liste
  lists : any[];
  listeSubscription : Subscription;
 

  constructor(private _route: ActivatedRoute, private _router: Router, private OneListService : ListService) { }

  ngOnInit() {
    //fait le lien avec l'id de la liste récupéré
    let id = this._route.snapshot.params['id'];
    this.listId = `${id}`;
    this.listeSubscription = this.OneListService.listSubject.subscribe( //inscription au service listService pour remplir la liste 
      (listes : any[]) => { this.lists = listes; });
      this.OneListService.emitListes(); //émet le sujet : affiche la liste (envoi au HTML)
  }

  goBack() {
    this._router.navigate(['/list']); //pour le bouton retour : renvoie à la page affichage de listes (!!sans modif)
}

// Créer une fonction "valider les modifs" : retour à la liste enti§re avec la mise à jour

}
