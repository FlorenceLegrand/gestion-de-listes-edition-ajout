import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAddFormComponent } from './list-add-form.component';

describe('ListAddFormComponent', () => {
  let component: ListAddFormComponent;
  let fixture: ComponentFixture<ListAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
